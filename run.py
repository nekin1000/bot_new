#!/usr/bin/env python
import asyncio
import importlib
from sys import argv, exit

from aiogram.utils import executor

from settings import dp, db_engine, apps, exception_handler
from utils.script_executor import execute

_, *args = argv


if __name__ == "__main__":
    db_engine.connect()

    if args:
        for arg in args:
            execute(arg)
        exit()

    for app in apps:
        try:
            globals().update(vars(importlib.import_module(f"apps.{app}.handlers")))
        except ModuleNotFoundError:
            print(f"Handlers for app {app} not found")
    loop = asyncio.get_event_loop()
    loop.set_exception_handler(exception_handler)
    executor.start_polling(dp)
else:
    # for alembic
    for app in apps:
        try:
            globals().update(vars(importlib.import_module(f"apps.{app}.models")))
        except ModuleNotFoundError:
            print(f"models for app {app} not found")
