import asyncio
import os

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from sqlalchemy import create_engine

from utils.logs import get_logger, get_asyncio_exception_handler

BOT_TOKEN = os.environ["TOKEN"]
MUSIC_CHAT_ID = os.environ.get("MUSIC_CHAT_ID", None)
ADMIN_ID = int(os.environ["ADMIN_ID"])
CRYPTO_API_KEY = os.environ.get("CRYPTO_API_KEY", None)
WEATHER_KEY = os.environ.get("WEATHER_KEY", None)
DEBUG = bool(int(os.environ.get("DEBUG", 1)))


storage = MemoryStorage()
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot, storage=storage)

db_engine = create_engine("sqlite:///db/BotDB.db")


apps = [
    "crypto",
    "task",
    "user",
    "weather",
    # "budget",
    "jarvis",
]

logger = get_logger(enabled=not DEBUG, bot=bot, admin_ids=[ADMIN_ID])
exception_handler = get_asyncio_exception_handler(logger)
