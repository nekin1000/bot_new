import asyncio
import logging
import sys

from aiogram import Bot


class NotificationHandler(logging.Handler):
    """Custom logs handler to send log messages to GraphQL API."""

    def __init__(self, bot: Bot, admin_ids: list[int], enabled: bool = False, level=logging.NOTSET):
        super().__init__(level)
        self.bot = bot
        self.admin_ids = admin_ids
        self.enabled = enabled

    @staticmethod
    def admin_error_message(file_name: str, exc_text: str):
        text = "Error!" + f"\nFunc name: {file_name}" + f"\nException: {exc_text}"
        return text

    def emit(self, record: logging.LogRecord):
        if not self.enabled:
            return
        asyncio.create_task(self.send_notification_to_admin(record))

    async def send_notification_to_admin(self, record: logging.LogRecord):
        for admin_id in self.admin_ids:
            await self.bot.send_message(
                admin_id,
                self.admin_error_message(record.funcName, str(record.exc_info[1])),
            )


class FileHandler(logging.FileHandler):
    def __init__(self, filename, mode="a", encoding=None, delay=False, errors=None, enabled=False):
        super().__init__(filename, mode=mode, encoding=encoding, delay=delay, errors=errors)
        self.enabled = enabled

    def emit(self, record: logging.LogRecord) -> None:
        if self.enabled:
            super().emit(record)


def get_logger(bot: Bot, admin_ids: list[int], enabled: bool = False) -> logging.Logger:
    file_handler = FileHandler(filename="logs/log.txt", enabled=enabled)
    file_handler.setLevel(logging.ERROR)
    notification_handler = NotificationHandler(level=logging.ERROR, enabled=enabled, bot=bot, admin_ids=admin_ids)

    logger = logging.getLogger("bot_logger")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)
    logger.addHandler(logging.StreamHandler())
    logger.addHandler(notification_handler)

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        logger.exception("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception

    return logger


def get_asyncio_exception_handler(logger: logging.Logger):
    def asyncio_exception_handler(loop, context):
        exception: Exception = context.get("exception", None)
        if exception:
            exc_info = (type(exception), exception, exception.__traceback__)
            if issubclass(exception.__class__, KeyboardInterrupt):
                return
            logger.exception("Uncaught exception", exc_info=exc_info)
        else:
            logger.exception(context["message"])

    return asyncio_exception_handler
