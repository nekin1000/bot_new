from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from apps.user.models import User
from settings import ADMIN_ID


button_menu = InlineKeyboardButton("Меню", callback_data="menu")
button_cryptocurrency = InlineKeyboardButton("💼 Криптовалюты 💼", callback_data="cryptocurrency")
button_tasks = InlineKeyboardButton("🗓 Задачи 🗓", callback_data="tasks")

button_ethereum_check = InlineKeyboardButton("Ethereum", callback_data="check_etherium")
button_xrp_check = InlineKeyboardButton("XRP", callback_data="check_xrp")
button_bitcoin_check = InlineKeyboardButton("Bitcoin", callback_data="check_bitcoin")

button_new_city = InlineKeyboardButton("Сменить город", callback_data="new_city")


def main_keyboard() -> InlineKeyboardMarkup:
    update = InlineKeyboardButton("🔄 Обновить 🔄", callback_data="menu")
    button_weather = InlineKeyboardButton("🌤 Погода 🌤", callback_data="weather")

    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(button_cryptocurrency, button_tasks, button_weather, update)

    return keyboard


def cryptocurrency(user_id: int) -> InlineKeyboardMarkup:
    button_history = InlineKeyboardButton("💾 История 💾", callback_data="history")
    button_calculation = InlineKeyboardButton("📈 Расчет 📈", callback_data="calculation")
    button_admin_xrp = InlineKeyboardButton("📊 XRP 📊", callback_data="admin_XRP")
    button_my_wallets = InlineKeyboardButton("📊 Кошельки 📊", callback_data="my_wallets")
    button_rates = InlineKeyboardButton("📊 Курсы 📊", callback_data="rates")

    keyboard = InlineKeyboardMarkup(row_width=1)

    if user_id == ADMIN_ID:
        keyboard.add(button_admin_xrp)

    keyboard.add(button_calculation, button_rates)
    if User().get({"telegram_id": user_id}).task_unique_code:
        keyboard.add(button_my_wallets)
    keyboard.add(button_history, button_menu)
    return keyboard


def back_from_crypto(save: bool = False) -> InlineKeyboardMarkup:
    button_remember = InlineKeyboardButton("Запомнить", callback_data="save_crypto")
    keyboard = InlineKeyboardMarkup(row_width=1)
    if save:
        keyboard.add(button_remember)
    keyboard.add(button_cryptocurrency, button_menu)
    return keyboard


AnswerPostKeyboard = InlineKeyboardMarkup(row_width=2)
AnswerPostKeyboard.add(button_ethereum_check, button_xrp_check, button_bitcoin_check)
AnswerPostKeyboard.add(button_menu)


def task_menu(user_id: int) -> InlineKeyboardMarkup:
    TaskMenuKeyboard = InlineKeyboardMarkup(row_width=1)
    create_task = InlineKeyboardButton("Создать", callback_data="create_task")
    connect_to_site = InlineKeyboardButton("Обновить код", callback_data="connect_to_site")
    disconnect_from_site = InlineKeyboardButton("Оключить", callback_data="disconnect_from_site")
    buttons = [connect_to_site, button_menu]
    if User().get({"telegram_id": user_id}).task_unique_code:
        buttons.insert(0, create_task)
        buttons.insert(2, disconnect_from_site)
    TaskMenuKeyboard.add(*buttons)
    return TaskMenuKeyboard


BackToTaskMenuKeyboard = InlineKeyboardMarkup(row_width=1)
BackToTaskMenuKeyboard.add(button_tasks, button_menu)

Menu = InlineKeyboardMarkup(row_width=1)
Menu.add(button_menu)

Weather = InlineKeyboardMarkup(row_width=1)
Weather.add(button_new_city)
Weather.add(button_menu)
