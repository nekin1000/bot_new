import logging
import random
from typing import Optional

from requests import ConnectTimeout, ReadTimeout

from apps.crypto.models import CryptoCurrency, CryptoHistory
from apps.crypto.services.api_queries import (
    convert_usd_rub,
    get_rate_usd,
    CryptoCurrencyRates,
)
from apps.user.services import get_user_city
from apps.weather.services import WeatherType, get_weather_by_api


user_error_message = "Error, message was send to developers, we will fix it so soon"

cryptocurrency_main = "Тут можно узнать курсы криптовалют, сделать расчет и сохранить историю"

task_menu_msg = "Это меню задач, отсюда можно создать новую задачу или подключиться к сайту easy-note.ru"
task_created_success = "Задача создана успешно"
connect_to_site_success = "Подключение успешно"


def msg_main(user_id: int) -> str:
    message = "Привет!"
    try:
        message += f"\n\nUSD/RUB: {get_rate_usd()}"
    except Exception as exc:
        logging.exception(exc)

    if city_name := get_user_city(user_id):
        try:
            weather_data = get_weather_by_api(city_name)
            message += f"\n\nПогода {weather_data.city}: {weather_data.temperature}°C"
        except (ConnectTimeout, ReadTimeout):
            message += "\n\nНе удалось загрузить погоду"
        except Exception as exc:
            logging.exception(exc)
    return message


def msg_crypto_rates(crypto_currencies: list[CryptoCurrency], usd_rate: float):
    text = ""
    try:
        for crypto_currency in crypto_currencies:
            text += f"Курс {crypto_currency.name}: ${crypto_currency.get_actual_rate()}\n"
        text += f"Курс usd: {usd_rate}"
    except Exception as e:
        logging.exception(e)
        return user_error_message
    return text


def msg_crypto_currency_info(crypto_currency: CryptoCurrency, amount: float) -> str:
    try:
        rate_usd = crypto_currency.get_actual_rate()
        rate_rub = convert_usd_rub(rate_usd)
        text = f"""\
                Курс Криптовалюты: ${rate_usd}
                \n {amount} ({crypto_currency.name}) -> {rate_rub * amount} ₽
            """
    except Exception as e:
        logging.exception(e)
        text = user_error_message
    return text


def msg_crypto_wallets_info(wallets: list[CryptoCurrencyRates]) -> str:
    text = "\n".join([f"{wallet.amount} {wallet.name}: {wallet.rate_usd} $, {wallet.rate_rub} ₽" for wallet in wallets])
    return text


def create_weather_message(city_name: str) -> str:
    try:
        weather_data: WeatherType = get_weather_by_api(city_name)
        answer = (
            f"Температура: {weather_data.temperature}°C,"
            + f"\nОщущается как: {weather_data.temperature_feels}°C,\nПриятного дня!"
        )
    except (ConnectTimeout, ReadTimeout):
        answer = "Сервер не отвечает, попробуйте позднее"
    except Exception as exc:
        logging.exception(exc)
        answer = user_error_message
    return answer


def create_history_msg(histories: Optional[list[CryptoHistory]]) -> str:
    result = "\n".join(
        [
            f"{history.date_created.strftime('%Y-%m-%d')}: {history.amount}({history.crypto_currency.name}) ->"
            + f" {history.price()} Р\n"
            for history in histories
        ]
    )
    return result or "Вы ещё ничего не сохранили"


def remember_answer():
    list_message = [
        "Если не забуду, дам знать",
        "Жди и надейся",
        "Постараюсь напомнить вовремя",
        "Если это так важно, то и напоминать не надо",
    ]
    random_answer = random.choice(list_message)
    return random_answer


def what_i_can(admin: bool = False):
    answer = "Я умею: \nпоказывать погоду;\nНапоминать;\nпосылать всех;\nвыбрать жертву"
    if admin:
        answer += "\nджарвис+=(погод, напомни, пошли всех, пошли их, выбери жертву), время сервера"
    return answer


help_message = (
    "Бот работает в двух режимах:"
    + "\n1) в переписке с самим ботом;"
    + "\nдоступна работа с криптовалютой"
    + "\nможно узнать погоду города Оренбурга"
    + "\n2) при добавлении в чат, группу, можно писать боту, как ассистенту"
    + "\nего возможности можно узнать, написав:"
    + "\n'джарвис, что ты умеешь?'"
)
