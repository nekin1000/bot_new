from datetime import datetime


class CacheCore:
    cache = {}
    cache_time = 30

    @classmethod
    def cache_wrapper(cls, func):
        def wrapper(*args, **kwargs):
            name_cache = func.__name__ + str(args) + str(kwargs)
            if name_cache in cls.cache.keys():
                cache = cls.cache[name_cache]
                difference_time = (datetime.now() - cache["date"]).seconds
                if difference_time < cls.cache_time:
                    return cache["data"]
            result_func = func(*args, **kwargs)
            cls.cache[name_cache] = {"data": result_func, "date": datetime.now()}
            return result_func

        return wrapper
