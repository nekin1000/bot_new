from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import sessionmaker, declarative_base

from settings import db_engine

Base = declarative_base()


class BaseModel:
    def save(self) -> None:
        Session = sessionmaker(db_engine)
        with Session() as session:
            session.add(self)
            session.commit()

    def filter_dict_by_self(self) -> dict:
        return {
            key: value
            for key, value in self.__dict__.items()
            if not key.startswith("_")
        }

    def get(self, filter_dict: dict = None):
        Session = sessionmaker(db_engine)
        if not filter_dict:
            filter_dict = self.filter_dict_by_self()

        with Session() as session:
            try:
                query = session.query(self.__class__).filter_by(**filter_dict).one()
            except NoResultFound:
                raise NoResultFound(
                    f"No row was found for {self.__class__} by filter: {filter_dict}"
                )
        return query

    def update_or_create(self, default_values: dict = None):
        filter_obj = self.filter_dict_by_self()

        for default_value in default_values.keys():
            filter_obj.pop(default_value, None)

        try:
            obj = self.get()
        except NoResultFound:
            obj = self

        for key, value in default_values.items():
            setattr(obj, key, value)
        obj.save()

    def update(self, default_values: dict = None):
        filter_obj = self.filter_dict_by_self()

        for default_value in default_values.keys():
            filter_obj.pop(default_value, None)

        try:
            obj = self.get()
        except NoResultFound:
            return

        for key, value in default_values.items():
            setattr(obj, key, value)
        obj.save()
