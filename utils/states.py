from aiogram.dispatcher.filters.state import State, StatesGroup


class WeatherState(StatesGroup):
    city_name = State()


class CryptoState(StatesGroup):
    value = State()


class TaskState(StatesGroup):
    title = State()
    connect_to_site = State()
