import importlib


def execute(module_name: str):
    try:
        script = importlib.import_module(f"scripts.{module_name}")
        script.run()
    except ModuleNotFoundError:
        print(f"Script with name {module_name} doesn't exist")
