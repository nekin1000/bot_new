from models.user import User
from settings import db_engine
from sqlalchemy.orm import sessionmaker

from models import crypto, budget

Session = sessionmaker(db_engine)


def create_user():
    with Session() as session:
        user_1 = User(
            telegram_id=112,
            username="megatapor_first",
            chat_id=121,
            active_status=True,
            first_name="megatapor",
            last_name="first",
            email="megatapor_first@mail.ru",
        )
        session.add(user_1)
        session.commit()


def create_crypto():
    with Session() as session:
        crypto_cur = crypto.CryptoCurrency(name="Bitcoin")
        session.add(crypto_cur)
        session.commit()

        user = session.query(User).get(1)

        history = crypto.CryptoHistory(value=99, rate=0.12, name=crypto_cur, user=user)
        session.add(history)
        session.commit()


def query_users():
    with Session() as session:
        queries = session.query(User).all()
        for query in queries:
            print("-" * 30)
            print(query.username)
        # print(query)


def query_crypto():
    with Session() as session:
        # queries = session.query(crypto.CryptoHistory).all()
        # for query in queries:
        #     print("-" * 30)
        #     print(query.name.name)
        # query = session.query(crypto.CryptoHistory).get({"id": 1})
        # print(query.name.name)
        query = session.query(crypto.CryptoCurrency).get({"id": 1})
        print("-" * 120)
        print(query)
        print("-" * 120)
        print(f"date_created: {query.crypto_histories[0].date_created}")


def run():
    # create_crypto()
    query_crypto()
    # create_user()
    # query_users()
