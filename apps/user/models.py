from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean, VARCHAR

from utils.database import Base, BaseModel


class User(Base, BaseModel):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)

    telegram_id = Column(Integer, unique=False, nullable=False)
    task_unique_code = Column(VARCHAR(20))
    username = Column(String(255), unique=True, nullable=True)
    active_status = Column(Boolean, default=True)

    first_name = Column(String(255), nullable=True)
    last_name = Column(String(255), nullable=True)
    email = Column(String(255), nullable=True)
    date_created = Column(DateTime, default=datetime.utcnow)

    city_name = Column(VARCHAR(100), nullable=True)

    def __repr__(self):
        return f"id: {self.id}, username: {self.username}"
