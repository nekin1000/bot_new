from aiogram import types
from aiogram.dispatcher import FSMContext

from utils.messages import msg_main, help_message
from settings import ADMIN_ID, dp, bot, MUSIC_CHAT_ID
from apps.user.services import create_user_from_message
from utils import KeyBoards


@dp.message_handler(commands=["start"])
async def start_handler(message: types.Message):
    create_user_from_message(message)
    user_id = message.from_user.id
    await bot.send_message(
        user_id,
        msg_main(user_id=user_id),
        reply_markup=KeyBoards.main_keyboard(),
    )


@dp.message_handler(commands=["help"])
async def help_handler(message: types.Message):
    user_id = message.from_user.id
    await bot.send_message(user_id, help_message, reply_markup=KeyBoards.Menu)


@dp.callback_query_handler(lambda c: c.data == "menu", state="*")
async def back_to_menu(callback: types.CallbackQuery, state: FSMContext):
    await bot.edit_message_text(
        "Грузим",
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.main_keyboard(),
    )
    current_state = await state.get_state()
    if current_state:
        await state.finish()
    msg = msg_main(callback.from_user.id)
    await bot.edit_message_text(
        msg,
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.main_keyboard(),
    )


# For Music
@dp.message_handler(content_types=["audio"])
async def music_handler(message: types.Message):
    user_id = message.from_user.id

    if user_id == ADMIN_ID and MUSIC_CHAT_ID:
        id_message = message.message_id
        chat_id = message.chat.id
        await bot.delete_message(chat_id, id_message)
        await bot.send_audio(int(MUSIC_CHAT_ID), message.audio.file_id)
