from typing import Union, Optional

from aiogram import types
from aiogram.types.user import User as AiogramUser
from sqlalchemy.exc import NoResultFound

from apps.user.models import User


def create_user_from_message(
    message: Union[types.Message, types.CallbackQuery]
) -> None:
    user: AiogramUser = message.from_user

    User(telegram_id=user.id).update_or_create(
        {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
        }
    )


def get_user_city(user_id: int) -> Optional[str]:
    try:
        user = User(telegram_id=user_id).get()
    except NoResultFound:
        return None
    return user.city_name
