from typing import NamedTuple

import requests

from utils.cache import CacheCore
from settings import WEATHER_KEY


class WeatherType(NamedTuple):
    city: str
    temperature: float
    temperature_feels: float


@CacheCore.cache_wrapper
def get_weather_by_api(city_name: str) -> WeatherType:
    url = "https://api.openweathermap.org/data/2.5/weather"
    if not WEATHER_KEY:
        raise ValueError("Weather api key missed")
    params = {"units": "metric", "q": city_name, "APPID": WEATHER_KEY}
    response = requests.get(url, params=params, timeout=0.5)

    if response.status_code != 200:
        raise ValueError(str(response.json()))

    response = response.json()
    return WeatherType(
        city=city_name,
        temperature=response["main"]["temp"],
        temperature_feels=response["main"]["feels_like"],
    )
