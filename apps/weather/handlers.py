from aiogram import types
from aiogram.dispatcher import FSMContext

from apps.user.models import User
from utils.messages import create_weather_message
from utils.states import WeatherState
from settings import dp, bot
from apps.user.services import create_user_from_message, get_user_city
from utils import KeyBoards


@dp.callback_query_handler(lambda c: c.data == "weather")
async def weather_handler(callback: types.CallbackQuery):
    create_user_from_message(callback)
    user_id = callback.from_user.id
    city_name = get_user_city(user_id)

    if city_name:
        answer = create_weather_message(city_name)
    else:
        answer = "Город не установлен"

    await bot.edit_message_text(
        answer,
        callback.message.chat.id,
        callback.message.message_id,
        reply_markup=KeyBoards.Weather,
    )


@dp.callback_query_handler(lambda c: c.data == "new_city")
async def new_city_start(callback: types.CallbackQuery, state: FSMContext):
    answer = "Введите название города:"
    async with state.proxy() as data:
        data["to_delete_msg"] = callback.message.message_id
    await WeatherState.city_name.set()
    await bot.edit_message_text(
        answer,
        callback.message.chat.id,
        callback.message.message_id,
        reply_markup=KeyBoards.Menu,
    )


@dp.message_handler(state=WeatherState.city_name)
async def weather_handler(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        to_delete_msg: int = data["to_delete_msg"]
    city_name = message.text
    answer = create_weather_message(city_name)

    user_id = message.from_user.id
    User(telegram_id=user_id).update_or_create({"city_name": city_name})
    await state.finish()

    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, to_delete_msg)
    await bot.send_message(user_id, answer, reply_markup=KeyBoards.Weather)
