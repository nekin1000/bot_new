import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float, Text
from sqlalchemy.orm import relationship

from utils.database import Base, BaseModel


class BudgetCategory(Base, BaseModel):
    __tablename__ = "budget_category"

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    description = Column(Text, nullable=True)

    user_budgets = relationship("UserBudget", back_populates="category")


class UserBudget(Base, BaseModel):
    __tablename__ = "user_budget"

    id = Column(Integer, primary_key=True)

    user_id = Column(Integer, ForeignKey("user.id"), nullable=True)
    # user = relationship("User", back_populates="user_budgets")
    user = relationship("User")

    name = Column(String(255))
    change_value = Column(Float, nullable=False)
    date = Column(DateTime, default=datetime.datetime.now())
    date_created = Column(DateTime, default=datetime.datetime.now())

    category_id = Column(Integer, ForeignKey("budget_category.id"))
    category = relationship("BudgetCategory", back_populates="user_budgets")
