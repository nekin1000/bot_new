from decimal import Decimal
from typing import NamedTuple, Optional

import requests

from apps.user.models import User
from settings import CRYPTO_API_KEY
from utils.cache import CacheCore


class CryptoCurrencyRates(NamedTuple):
    name: str
    rate_usd: Decimal
    rate_rub: Optional[Decimal] = Decimal("0")
    amount: Optional[Decimal] = Decimal("0")


def get_wallets_info(user_id: int) -> list[CryptoCurrencyRates]:
    user = User().get({"telegram_id": user_id})
    unique_code = user.task_unique_code
    url = "https://easy-note.ru/temp/bot/get-crypto-info/"
    data = {"user_id": user_id, "unique_code": unique_code}
    request = requests.post(url, data=data)
    if request.status_code != 200:
        raise ValueError("Some gone wrong")
    wallets: list[CryptoCurrencyRates] = [
        CryptoCurrencyRates(
            name=wallet["name"], rate_usd=wallet["rate_usd"], rate_rub=wallet["rate_rub"], amount=wallet["amount"]
        )
        for wallet in request.json()["wallets"]
    ]
    return wallets


@CacheCore.cache_wrapper
def get_cryptocurrency_rate(crypto_id: int) -> float:
    url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest"
    headers = {
        "Accepts": "application/json",
        "X-CMC_PRO_API_KEY": CRYPTO_API_KEY,
    }
    response = requests.get(url, params={"id": crypto_id}, headers=headers)

    if response.status_code != 200:
        raise ValueError(response.json())

    response = response.json()
    price = response["data"][str(crypto_id)]["quote"]["USD"]["price"]
    return round(price, 2)


def convert_usd_rub(price: float) -> float:
    return round(get_rate_usd() * price, 1)


@CacheCore.cache_wrapper
def get_rate_usd() -> float:
    response = requests.get("https://www.cbr-xml-daily.ru/latest.js")

    if response.status_code != 200:
        raise ValueError(response.json())

    response = response.json()
    price = 1 / response["rates"]["USD"]
    return round(price, 2)
