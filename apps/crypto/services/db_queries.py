from datetime import date

from sqlalchemy.orm import sessionmaker, joinedload

from apps.crypto.models import CryptoCurrency, CryptoHistory
from settings import db_engine


def get_top_cryptocurrency(cnt: int) -> list[CryptoCurrency]:
    Session = sessionmaker(db_engine)
    with Session() as session:
        cryptocurrencies = session.query(CryptoCurrency).limit(cnt).all()
    return cryptocurrencies


def get_user_crypto_history(
    user_id: int, date_start: date = None, date_end: date = None
) -> list[CryptoHistory]:
    Session = sessionmaker(db_engine)
    with Session() as session:
        histories = (
            session.query(CryptoHistory)
            .options(joinedload(CryptoHistory.crypto_currency))
            .filter(CryptoHistory.user_id == user_id)
            .all()
        )
    return histories
