from aiogram import types
from aiogram.dispatcher import FSMContext

from apps.crypto.models import CryptoHistory, CryptoCurrency
from apps.crypto.services.api_queries import get_rate_usd, CryptoCurrencyRates, get_wallets_info
from apps.crypto.services.db_queries import get_top_cryptocurrency, get_user_crypto_history
from utils.messages import (
    msg_crypto_rates,
    msg_crypto_currency_info,
    create_history_msg,
    cryptocurrency_main,
    user_error_message,
    msg_crypto_wallets_info,
)
from settings import dp, bot
from utils import KeyBoards
from utils.states import CryptoState


@dp.callback_query_handler(lambda c: c.data == "cryptocurrency")
async def crypto_currency_handler(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await bot.edit_message_text(
        cryptocurrency_main,
        chat_id=user_id,
        message_id=callback.message.message_id,
        reply_markup=KeyBoards.cryptocurrency(user_id),
    )


@dp.callback_query_handler(lambda c: c.data == "save_crypto")
async def fix_last_message(callback_query: types.CallbackQuery):
    user_id = callback_query.from_user.id
    CryptoHistory().save_from_message(callback_query.message.text, user_id)
    await bot.edit_message_reply_markup(
        user_id,
        callback_query.message.message_id,
        reply_markup=KeyBoards.back_from_crypto(),
    )


@dp.callback_query_handler(lambda c: c.data == "rates")
async def crypto_rates(callback: types.CallbackQuery):
    crypto_currencies = get_top_cryptocurrency(cnt=3)
    usd_rate = get_rate_usd()
    answer = msg_crypto_rates(crypto_currencies, usd_rate)
    await bot.edit_message_text(
        answer,
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.back_from_crypto(),
    )


@dp.callback_query_handler(lambda c: c.data == "history")
async def history_handler(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    histories = get_user_crypto_history(user_id)
    answer = create_history_msg(histories)
    await bot.edit_message_text(
        text=answer,
        chat_id=user_id,
        message_id=callback.message.message_id,
        reply_markup=KeyBoards.back_from_crypto(),
    )


@dp.callback_query_handler(lambda c: c.data == "calculation")
async def calculation(callback: types.CallbackQuery):
    await bot.edit_message_text(
        "Выберите криптовалюту:",
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.AnswerPostKeyboard,
    )


@dp.callback_query_handler(lambda c: c.data.startswith("check"))
async def crypto_currency_check(callback_query: types.CallbackQuery, state: FSMContext):
    crypto_name = callback_query.data[6:]
    async with state.proxy() as data:
        data["name"] = crypto_name
        data["msg_id_to_delete"] = callback_query.message.message_id
    await CryptoState.value.set()
    await bot.edit_message_text(
        "Количество выбранной криптовалюты:",
        chat_id=callback_query.from_user.id,
        message_id=callback_query.message.message_id,
        reply_markup=KeyBoards.Menu,
    )


@dp.message_handler(state=CryptoState.value)
async def crypto_calc(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        name: str = data["name"]
        msg_id_to_delete: int = data["msg_id_to_delete"]
    await bot.delete_message(message.from_user.id, message.message_id)
    amount = float(message.text.replace(",", "."))
    crypto_currency = CryptoCurrency(name=name).get()
    answer = msg_crypto_currency_info(crypto_currency, amount=amount)
    keyboard = KeyBoards.back_from_crypto(save=False if "Error" in answer else True)
    await state.finish()
    await bot.delete_message(message.from_user.id, msg_id_to_delete)
    await bot.send_message(message.chat.id, answer, reply_markup=keyboard)


@dp.callback_query_handler(lambda c: c.data == "admin_XRP")
async def admin_xrp(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    crypto_currency = CryptoCurrency(name="xrp").get()
    answer = msg_crypto_currency_info(crypto_currency, amount=20)
    await bot.edit_message_text(
        answer,
        user_id,
        callback.message.message_id,
        reply_markup=KeyBoards.back_from_crypto(save=True),
    )


@dp.callback_query_handler(lambda c: c.data == "my_wallets")
async def my_wallets(callback: types.CallbackQuery):
    user_id = callback.from_user.id

    try:
        wallets: list[CryptoCurrencyRates] = get_wallets_info(user_id=user_id)
        text = msg_crypto_wallets_info(wallets=wallets)
    except:
        text = user_error_message
    await bot.edit_message_text(
        text,
        user_id,
        callback.message.message_id,
        reply_markup=KeyBoards.back_from_crypto(save=False),
    )
