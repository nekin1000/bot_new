import datetime
import re
from typing import Optional, Callable

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float
from sqlalchemy.orm import relationship

from apps.crypto.services.api_queries import convert_usd_rub, get_cryptocurrency_rate
from utils.database import Base, BaseModel


class CryptoCurrency(Base, BaseModel):
    __tablename__ = "crypto_currency"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    api_id = Column(Integer, unique=True, nullable=False)   # cryptocurrency id from coinmarketcap

    crypto_histories = relationship("CryptoHistory", back_populates="crypto_currency")

    def get_actual_rate(self) -> float:
        return get_cryptocurrency_rate(self.api_id)


class CryptoHistory(Base, BaseModel):
    __tablename__ = "crypto_history"

    id = Column(Integer, primary_key=True)
    amount = Column(Float, default=0)
    rate = Column(Float, nullable=False)  # cryptocurrency rate
    date_created = Column(DateTime, default=datetime.datetime.now())

    crypto_currency_id = Column(Integer, ForeignKey("crypto_currency.id"), nullable=False)
    crypto_currency = relationship("CryptoCurrency", back_populates="crypto_histories")

    user_id = Column(Integer, ForeignKey("user.id"), nullable=True)

    def save_from_message(self, message: str, user_id: int = None) -> "CryptoHistory":
        crypto_rate, crypto_amount, crypto_result = [float(value) for value in re.findall(r"-?\d+\.?\d*", message)]
        crypto_name = re.search(r"\(.*\)", message)[0][1:-1]

        crypto_currency = CryptoCurrency(name=crypto_name).get()

        if user_id:
            self.user_id = user_id
        self.amount = crypto_amount
        self.rate = crypto_rate
        self.crypto_currency = crypto_currency
        self.save()
        return self

    def price(self, convert_method: Optional[Callable] = convert_usd_rub) -> float:
        crypto_price_usd = self.amount * self.rate
        return convert_method(crypto_price_usd)

    def __repr__(self):
        return f"id: {self.id}, date: {self.date_created}"
