from aiogram import types
from aiogram.dispatcher import FSMContext

from apps.task.services import connect_user_to_site, create_task, disconnect_user_from_site
from utils.messages import task_menu_msg, task_created_success, connect_to_site_success
from settings import dp, bot
from utils import KeyBoards
from utils.states import TaskState


@dp.callback_query_handler(lambda c: c.data == "tasks", state="*")
async def task_menu(callback: types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state:
        await state.finish()
    await bot.edit_message_text(
        task_menu_msg,
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.task_menu(user_id=callback.from_user.id),
    )


@dp.callback_query_handler(lambda c: c.data == "connect_to_site", state="*")
async def connect_to_site_button(callback: types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state:
        await state.finish()
    await TaskState.connect_to_site.set()
    await bot.edit_message_text(
        "Введите код с сайта",
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.BackToTaskMenuKeyboard,
    )


@dp.callback_query_handler(lambda c: c.data == "disconnect_from_site", state="*")
async def disconnect_from_site_button(callback: types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state:
        await state.finish()
    user_id = callback.from_user.id
    disconnect_user_from_site(user_id=user_id)
    await bot.edit_message_text(
        "Успешно",
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.BackToTaskMenuKeyboard,
    )


@dp.message_handler(state=TaskState.connect_to_site)
async def connect_to_site_result(message: types.Message, state: FSMContext):
    await bot.delete_message(message.from_user.id, message.message_id)
    await state.finish()
    user_id = message.from_user.id
    connect_user_to_site(user_id=user_id, unique_code=message.text)
    await bot.send_message(message.chat.id, connect_to_site_success, reply_markup=KeyBoards.BackToTaskMenuKeyboard)


@dp.callback_query_handler(lambda c: c.data == "create_task", state="*")
async def create_task_button(callback: types.CallbackQuery, state: FSMContext):
    current_state = await state.get_state()
    if current_state:
        await state.finish()
    async with state.proxy() as data:
        data["to_delete_msg"] = callback.message.message_id
    await TaskState.title.set()
    await bot.edit_message_text(
        "Введите текст задачи",
        callback.from_user.id,
        callback.message.message_id,
        reply_markup=KeyBoards.BackToTaskMenuKeyboard,
    )


@dp.message_handler(state=TaskState.title)
async def create_task_result(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        to_delete_msg: int = data["to_delete_msg"]
    user_id = message.from_user.id
    create_task(user_id=user_id, title=message.text)
    await state.finish()
    try:
        await bot.delete_message(message.from_user.id, message.message_id)
        await bot.delete_message(message.from_user.id, to_delete_msg)
    except:
        pass
    await bot.send_message(message.chat.id, task_created_success, reply_markup=KeyBoards.BackToTaskMenuKeyboard)
