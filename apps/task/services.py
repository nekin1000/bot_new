import requests

from apps.user.models import User


def connect_user_to_site(user_id: int, unique_code: str) -> None:
    User(telegram_id=user_id).update_or_create({"task_unique_code": unique_code})
    create_task(user_id=user_id, title="Test connection")


def disconnect_user_from_site(user_id: int) -> None:
    User(telegram_id=user_id).update_or_create({"task_unique_code": ""})


def create_task(user_id: int, title: str) -> None:
    if len(title) > 100:
        raise ValueError("Too long title")
    if len(title) < 3:
        raise ValueError("Too short title")

    user = User().get({"telegram_id": user_id})
    unique_code = user.task_unique_code
    url = "https://easy-note.ru/temp/bot/create-task/"
    data = {"user_id": user_id, "title": title, "unique_code": unique_code}
    request = requests.post(url, data=data)
    if request.status_code != 200:
        raise ValueError("Some gone wrong")
