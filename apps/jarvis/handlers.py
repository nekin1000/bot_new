from typing import Optional

from aiogram import types
import time

from apps.user.services import create_user_from_message, get_user_city
from utils import messages as msg
from settings import ADMIN_ID, dp, bot
from apps.jarvis.services import moderate
from utils.messages import create_weather_message


@dp.message_handler()
async def jarvis(message: types.Message):
    create_user_from_message(message)
    if moderate(message):
        await bot.delete_message(message.chat.id, message.message_id)
    else:
        user_name = message.from_user.first_name
        message_text = message.text.lower()
        user_id = message.from_user.id
        answer = jarvis_answer(message_text, user_name, user_id)
        if answer:
            await bot.send_message(message.chat.id, answer)


def jarvis_answer(message_text: str, user_name: str, user_id: int) -> Optional[str]:
    message_text = message_text.lower()
    if "джарвис" not in message_text:
        return None

    if "что ты умеешь" in message_text:
        return msg.what_i_can(user_id)
    if "время сервера" in message_text:
        return time.strftime("%H:%M", time.localtime())

    if check_welcome(message_text):
        return "Приветствую, {}".format(user_name)

    if "погод" in message_text:
        return get_weather_answer(user_id)

    if ("пошли всех" in message_text) or ("пошли их" in message_text):
        if user_id == ADMIN_ID:
            return "Да пошли вы все"
        else:
            return "\nСам пошел"
    return None


def check_welcome(message: str) -> bool:
    welcomes = ["прив", "здоров", "здаров", "шалом"]
    for welcome in welcomes:
        if welcome in message:
            return True
    return False


def get_weather_answer(user_id: int) -> str:
    city_name = get_user_city(user_id)
    if city_name:
        return create_weather_message(city_name)
    else:
        return "Укажите город в настройках"
