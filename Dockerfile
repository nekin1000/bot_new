FROM python:3.10

RUN python -m pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir poetry cmake

COPY poetry.lock poetry.lock
COPY pyproject.toml pyproject.toml

RUN poetry export -f requirements.txt --without-hashes --output requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /bot
COPY . /bot
